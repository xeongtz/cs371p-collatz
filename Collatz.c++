#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <algorithm> //min, max

#include "Collatz.h"

using namespace std;

int cache[1000001] = { 0 }; //init to 0

// ------------
// collatz_read
// ------------
pair<int, int> collatz_read(const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// Calculate Cycle Length
// ------------
inline int CalculateCycleLength(int num)
{
    assert(num > 0);

    int cycleLength = cache[num];
    if (cycleLength != 0)
    {
        return cycleLength;
    }
    cycleLength++;
    long long int temp = num;
    while (temp != 1)
    {
        if (temp % 2 == 0)
        {
            temp = temp >> 1;
        }
        else
        {
            temp = temp + (temp >> 1) + 1;
            cycleLength++;
        }
        cycleLength++;
    }
    cache[num] = cycleLength;
    assert(cycleLength > 0);
    return cycleLength;
}

// ------------
// collatz_eval
// ------------
int collatz_eval(int i, int j)
{
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);

    int rangeStart = min(i, j);
    int rangeStop = max(i, j);
    //if m = endpoint / 2 + 1 is greater than the startpoint, the range can start at m.
    //(any value from rangeStart to m can be multiplied by 2 and be in between m and rangeStop)
    int m = (rangeStop >> 2) + 1;
    if (rangeStart < m)
    {
        rangeStart = m;
    }

    int maxCycleLength = 1;
    for (int i = rangeStart; i < rangeStop + 1; i++)
    {
        int cycleLength = CalculateCycleLength(i);
        if (cycleLength > maxCycleLength)
        {
            maxCycleLength = cycleLength;
        }
    }

    assert(maxCycleLength > 0);
    return maxCycleLength;
}

// -------------
// collatz_print
// -------------

void collatz_print(ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve(istream& r, ostream& w)
{
    //metacache
    cache[1] = 1;
    cache[2] = 2;
    cache[3] = 8;
    cache[4] = 3;
    cache[5] = 6;
    cache[6] = 9;
    cache[7] = 17;
    cache[8] = 4;
    cache[9] = 20;
    cache[10] = 7;
    cache[11] = 15;
    cache[12] = 10;
    cache[13] = 10;
    cache[14] = 18;
    cache[15] = 18;
    cache[16] = 5;
    cache[17] = 13;
    cache[18] = 21;
    cache[19] = 21;
    cache[20] = 8;
    cache[21] = 8;
    cache[22] = 16;
    cache[23] = 16;
    cache[24] = 11;
    cache[25] = 24;
    cache[26] = 11;
    cache[27] = 112;
    cache[28] = 19;
    cache[29] = 19;
    cache[30] = 19;
    cache[31] = 107;
    cache[32] = 6;
    cache[33] = 27;
    cache[34] = 14;
    cache[35] = 14;
    cache[36] = 22;
    cache[37] = 22;
    cache[38] = 22;
    cache[39] = 35;
    cache[40] = 9;
    cache[64] = 7;
    cache[128] = 8;
    cache[256] = 9;
    cache[512] = 10;
    cache[1024] = 11;
    cache[2048] = 12;
    cache[4096] = 13;
    cache[8192] = 14;
    cache[16384] = 15;
    cache[32768] = 16;
    cache[65536] = 17;
    cache[262144] = 18;
    cache[524288] = 19;
    cache[97] = 119;
    cache[871] = 179;
    cache[6171] = 262;
    cache[77031] = 351;
    cache[837799] = 525;
    cache[54] = 113;
    cache[73] = 116;
    cache[97] = 119;
    cache[129] = 122;
    cache[171] = 125;
    cache[231] = 128;
    cache[313] = 131;
    cache[327] = 144;
    cache[649] = 145;
    cache[703] = 171;

    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}

/*
Hackerrank:
//#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <algorithm>

using namespace std;

int cache[1000001] = {0};

// ------------
// collatz_read
// ------------
pair<int, int> collatz_read (const string& s) {
istringstream sin(s);
int i;
int j;
sin >> i >> j;
return make_pair(i, j);}

// ------------
// Calculate Cycle Length
// ------------
inline int CalculateCycleLength(int num)
{
int cycleLength = cache[num];
if (cycleLength != 0)
{
return cycleLength;
}
cycleLength++;
long long int temp = num;
while (temp != 1)
{
if (temp % 2 == 0)
{
temp = temp >> 1;
}
else
{
temp = temp + (temp >> 1) + 1;
cycleLength++;
}
cycleLength++;
}
cache[num] = cycleLength;
return cycleLength;
}

// ------------
// collatz_eval
// ------------
int collatz_eval(int i, int j)
{
	int rangeStart = min(i, j);
	int rangeStop = max(i, j);
	//if m = endpoint / 2 + 1 is greater than the startpoint, the range can start at m.
	//(any value from rangeStart to m can be multiplied by 2 and be in between m and rangeStop)
	int m = (rangeStop >> 2) + 1;
	if (rangeStart < m)
	{
		rangeStart = m;
	}

	int maxCycleLength = 1;
	for (int i = rangeStart; i < rangeStop + 1; i++)
	{
		int cycleLength = CalculateCycleLength(i);
		if (cycleLength > maxCycleLength)
		{
			maxCycleLength = cycleLength;
		}
	}

	return maxCycleLength;
}

// -------------
// collatz_print
// -------------

void collatz_print(ostream& w, int i, int j, int v) {
	w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve(istream& r, ostream& w) {
	cache[1] = 1;
	cache[2] = 2;
	cache[3] = 8;
	cache[4] = 3;
	cache[5] = 6;
	cache[6] = 9;
	cache[7] = 17;
	cache[8] = 4;
	cache[9] = 20;
	cache[10] = 7;
	cache[11] = 15;
	cache[12] = 10;
	cache[13] = 10;
	cache[14] = 18;
	cache[15] = 18;
	cache[16] = 5;
	cache[17] = 13;
	cache[18] = 21;
	cache[19] = 21;
	cache[20] = 8;
	cache[21] = 8;
	cache[22] = 16;
	cache[23] = 16;
	cache[24] = 11;
	cache[25] = 24;
	cache[26] = 11;
	cache[27] = 112;
	cache[28] = 19;
	cache[29] = 19;
	cache[30] = 19;
	cache[31] = 107;
	cache[32] = 6;
	cache[33] = 27;
	cache[34] = 14;
	cache[35] = 14;
	cache[36] = 22;
	cache[37] = 22;
	cache[38] = 22;
	cache[39] = 35;
	cache[40] = 9;
	cache[64] = 7;
	cache[128] = 8;
	cache[256] = 9;
	cache[512] = 10;
	cache[1024] = 11;
	cache[2048] = 12;
	cache[4096] = 13;
	cache[8192] = 14;
	cache[16384] = 15;
	cache[32768] = 16;
	cache[65536] = 17;
	cache[262144] = 18;
	cache[524288] = 19;
	cache[97] = 119;
	cache[871] = 179;
	cache[6171] = 262;
	cache[77031] = 351;
	cache[837799] = 525;
	cache[54] = 113;
	cache[73] = 116;
	cache[97] = 119;
	cache[129] = 122;
	cache[171] = 125;
	cache[231] = 128;
	cache[313] = 131;
	cache[327] = 144;
	cache[649] = 145;
	cache[703] = 171;
	string s;
	while (getline(r, s)) {
		const pair<int, int> p = collatz_read(s);
		const int            i = p.first;
		const int            j = p.second;
		const int            v = collatz_eval(i, j);
		collatz_print(w, i, j, v);
	}
}


int main() {
	using namespace std;
	collatz_solve(cin, cout);
	return 0;
}


*/